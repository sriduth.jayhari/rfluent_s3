use rusoto_core::Region;
use rusoto_core::credential::EnvironmentProvider;
use rusoto_core::request::HttpClient;
use rusoto_s3::{S3Client, S3, PutObjectRequest, StreamingBody};
use std::env;

// Binary utility that pushes all buffers listed in the SQLite DB
fn main() {
    let request_dispatcher = HttpClient::new().unwrap();
    let credentials_provider = EnvironmentProvider::default();
    let region = Region::default();
 
    let client = S3Client::new_with(request_dispatcher,
                                    credentials_provider,
                                    region);
   
}
