#![allow(non_snake_case)]


use fluent_bit_rust::output;
use std::env;

use std::io::Write;

use std::path::Path;
use std::fs::File;

use std::process;
use std::mem;
use std::io::ErrorKind;
use std::io::Error;

use json::object::Object;

use rmp::decode::read_array_len;
  
#[macro_use]
extern crate lazy_static;

mod utils;
use utils::*;

#[no_mangle]
unsafe fn FLBPluginRegister(def: *mut output::FLBPluginProxyDef) -> libc::c_int {
    return output::FLBPluginRegister(def, "rfile", "StdOut.rs");
}

#[no_mangle]
unsafe fn FLBPluginInit(plugin: *mut output::FLBOutPlugin) -> libc::c_int {    
    match register_globals(plugin) {
        Err(error) => {
            println!("FATAL: plugin could not start. Cause : {}", error);
            return output::FLB_ERROR;
        },
        _ => ()
    }
    
    rotate_current_buffer();

    return output::FLB_OK   
}


#[no_mangle]
unsafe fn FLBPluginFlush(data: *const u8, length: libc::c_int, _tag: *const libc::c_char) -> libc::c_int {
    let data_u8: &[u8] = std::slice::from_raw_parts(data, length as usize);
    let mut cursor = std::io::Cursor::new(data_u8);

    let mut lines = String::new();
    
    while {
        let current_pos = cursor.position();
        current_pos < (length as u64)
    } {
        let _arr_len: u32 = read_array_len(&mut cursor).unwrap();
        let timestamp = rmpv::decode::value::read_value(&mut cursor).unwrap();
        let mut log_entry = rmpv::decode::value::read_value(&mut cursor).unwrap();
        
        let mut json = Object::new();

        for (k, v) in log_entry.as_map().unwrap() {
            json.insert(&k.as_str().unwrap_or(&"".to_string()), json::JsonValue::from(v.as_str().unwrap_or(&"".to_string())));        
        }

        lines = lines + &json::stringify(json) + "\n"
    }
    
    write_log_to_buffer(lines);
    return output::FLB_OK;
}



#[no_mangle]
extern "C" fn FLBPluginExit() -> libc::c_int {
    return output::FLB_OK;
}
